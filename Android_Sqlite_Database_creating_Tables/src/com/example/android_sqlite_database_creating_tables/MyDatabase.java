package com.example.android_sqlite_database_creating_tables;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Student.db";
    public static final String STUDENT_TABLE = "create table student(Id Text,Name Text,Marks Text,Course_name text)";
    SQLiteDatabase database;

    /*
     * SQLiteOpenHelper is an abstract class.
     */
    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    //Called when the database is created for the first time.
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(STUDENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS student");
        onCreate(sqLiteDatabase);
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        return super.getWritableDatabase();
    }

    @Override
    public synchronized void close() {
        super.close();
    }
}
